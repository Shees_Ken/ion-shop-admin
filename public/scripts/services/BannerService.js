angular.module('main').factory('Banner', function () {

    var Banner = Parse.Object.extend('Banner', {
  
      isActive: function () {
        return this.status === 'Active';
      },
  
      isInactive: function () {
        return this.status === 'Inactive';
      }
  
    }, {
  
      new() {
        return new Banner();
      },
  
      getInstance: function () {
        return this;
      },
  
      all: function (params) {
  
        var query = new Parse.Query(this);
  
        if (params && params.canonical) {
          query.contains('canonical', params.canonical);
        }
  
        if (params && params.sort) {
          query.equalTo('sort', params.sort);
        }
  
        if (params && params.limit && params.page) {
          query.limit(params.limit);
          query.skip((params.page * params.limit) - params.limit);
        }
  
        if (params.exclude) {
          query.notContainedIn('objectId', params.exclude);
        }
  
        if (params && params.orderBy === 'asc') {
          query.ascending(params.orderByField);
        } else if (params && params.orderBy === 'desc') {
          query.descending(params.orderByField);
        } else {
          query.descending('createdAt');
        }
  
        query.doesNotExist('deletedAt');
  
        return query.find();
  
      },
  
      count: function (params) {
  
        var query = new Parse.Query(this);
  
        if (params && params.canonical) {
          query.contains('canonical', params.canonical);
        }
  
        if (params && params.isFeatured) {
          query.equalTo('isFeatured', params.isFeatured);
        }
  
        query.doesNotExist('deletedAt');
  
        return query.count();
  
      },
  
      save: function (obj) {
        return obj.save();
      },
  
      delete: function (obj) {
        obj.deletedAt = new Date;
        // return obj.save();
      }
  
    });
  
  
    Object.defineProperty(Banner.prototype, 'name', {
      get: function () {
        return this.get('name');
      },
      set: function (val) {
        this.set('name', val);
      }
    });
  
    Object.defineProperty(Banner.prototype, 'sort', {
      get: function () {
        return this.get('sort');
      },
      set: function (val) {
        this.set('sort', val);
      }
    });
  
    Object.defineProperty(Banner.prototype, 'status', {
      get: function () {
        return this.get('status');
      },
      set: function (val) {
        this.set('status', val);
      }
    });
  
    Object.defineProperty(Banner.prototype, 'type', {
        get: function() {
            return this.get('type');
        },
        set: function (val) {
            this.set('type',val)
        }
    })

    Object.defineProperty(Banner.prototype, 'deletedAt', {
      get: function () {
        return this.get('deletedAt');
      },
      set: function (val) {
        this.set('deletedAt', val);
      }
    });
  
    Object.defineProperty(Banner.prototype, 'image', {
      get: function () {
        return this.get('image');
      },
      set: function (val) {
        this.set('image', val);
      }
    });
  
    Object.defineProperty(Banner.prototype, 'imageThumb', {
      get: function () {
        return this.get('imageThumb');
      },
      set: function (val) {
        this.set('imageThumb', val);
      }
    });
  
    return Banner;
  
  });