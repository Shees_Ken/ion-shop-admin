angular.module('main').factory('ComboItem', function () {

  var ComboItem = Parse.Object.extend('ComboItem', {

    initialize: function () {
      this.status = 'Active';
      this.images = [];
    }
  }, {
    new() {
      return new ComboItem();
    },

    getInstance: function () {
      return this;
    },

    all: function (params) {

      var query = new Parse.Query(this);

      if (params && params.canonical) {
        query.contains('canonical', params.canonical);
      }

      if (params && params.status) {
        query.equalTo('status', params.status);
      }

      if (params && params.limit && params.page) {
        query.limit(params.limit);
        query.skip((params.page * params.limit) - params.limit);
      }

      if (params.exclude) {
        query.notContainedIn('objectId', params.exclude);
      }

      if (params && params.orderBy == 'asc') {
        query.ascending(params.orderByField);
      } else if (params && params.orderBy == 'desc') {
        query.descending(params.orderByField);
      } else {
        query.descending('createdAt');
      }

      query.doesNotExist('deletedAt');

      return query.find();

    },

    count: function (params) {

      var query = new Parse.Query(this);

      if (params && params.canonical) {
        query.contains('canonical', params.canonical);
      }

      if (params && params.status) {
        query.equalTo('status', params.status);
      }

      query.doesNotExist('deletedAt');

      return query.count();
    },

    save: function (obj) {
      return obj.save();
    },

    delete: function (obj) {
      obj.deletedAt = new Date;
      return obj.save();
    }

  });

  Object.defineProperty(ComboItem.prototype, 'name', {
    get: function () {
      return this.get('name');
    },
    set: function (val) {
      this.set('name', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'category', {
    get: function () {
      return this.get('category');
    },
    set: function (val) {
      this.set('category', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'subcategory', {
    get: function () {
      return this.get('subcategory');
    },
    set: function (val) {
      this.set('subcategory', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'featuredImage', {
    get: function () {
      return this.get('featuredImage');
    },
    set: function (val) {
      this.set('featuredImage', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'images', {
    get: function () {
      return this.get('images');
    },
    set: function (val) {
      this.set('images', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'items', {
    get: function () {
      return this.get('items');
    },
    set: function (val) {
      this.set('items', val);
    }
  });

  // Object.defineProperty(ComboItem.prototype, 'price', {
  //   get: function () {
  //     return this.get('price');
  //   },
  //   set: function (val) {
  //     this.set('price', val);
  //   }
  // });

  // Object.defineProperty(ComboItem.prototype, 'salePrice', {
  //   get: function () {
  //     return this.get('salePrice');
  //   },
  //   set: function (val) {
  //     this.set('salePrice', val);
  //   }
  // });

  Object.defineProperty(ComboItem.prototype, 'isFeatured', {
    get: function () {
      return this.get('isFeatured');
    },
    set: function (val) {
      this.set('isFeatured', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'isNewArrival', {
    get: function () {
      return this.get('isNewArrival');
    },
    set: function (val) {
      this.set('isNewArrival', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'views', {
    get: function () {
      return this.get('views');
    },
    set: function (val) {
      this.set('views', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'deletedAt', {
    get: function () {
      return this.get('deletedAt');
    },
    set: function (val) {
      this.set('deletedAt', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'ratingTotal', {
    get: function () {
      return this.get('ratingTotal');
    },
    set: function (val) {
      this.set('ratingTotal', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'ratingCount', {
    get: function () {
      return this.get('ratingCount');
    },
    set: function (val) {
      this.set('ratingCount', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'ratingAvg', {
    get: function () {
      return this.get('ratingAvg');
    },
    set: function (val) {
      this.set('ratingAvg', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'likeCount', {
    get: function () {
      return this.get('likeCount');
    },
    set: function (val) {
      this.set('likeCount', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'tags', {
    get: function () {
      return this.get('tags');
    },
    set: function (val) {
      this.set('tags', val);
    }
  });
  
  Object.defineProperty(ComboItem.prototype, 'categories', {
    get: function () {
      return this.get('categories');
    },
    set: function (val) {
      this.set('categories', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'subcategories', {
    get: function () {
      return this.get('subcategories');
    },
    set: function (val) {
      this.set('subcategories', val);
    }
  });

  Object.defineProperty(ComboItem.prototype, 'status', {
    get: function () {
      return this.get('status');
    },
    set: function (val) {
      this.set('status', val);
    }
  });

  return ComboItem;

});