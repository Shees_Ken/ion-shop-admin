angular.module("main").factory("DeliveryTime", function() {
  var DeliveryTime = Parse.Object.extend(
    "DeliveryTime",
    {},
    {
      new() {
        return new DeliveryTime();
      },

      getInstance: function() {
        return this;
      },

      all: function(params) {
        var query = new Parse.Query(this);

        if (params && params.canonical) {
          query.contains("canonical", params.canonical);
        }

        if (params && params.limit && params.page) {
          query.limit(params.limit);
          query.skip(params.page * params.limit - params.limit);
        }

        if (params && params.orderBy === "asc") {
          query.ascending(params.orderByField);
        } else if (params && params.orderBy === "desc") {
          query.descending(params.orderByField);
        } else {
          query.descending("createdAt");
        }

        query.doesNotExist("deletedAt");

        return query.find();
      },

      count: function(params) {
        var query = new Parse.Query(this);

        if (params && params.canonical) {
          query.contains("canonical", params.canonical);
        }

        query.doesNotExist("deletedAt");

        return query.count();
      },

      getOne: function (objectId) {
        var query = new Parse.Query(DeliveryTime);
        return query.get(objectId);
      },

      save: function(obj) {
        return obj.save();
      },

      delete: function(obj) {
        obj.deletedAt = new Date();
        return obj.save();
      },
    }
  );

  Object.defineProperty(DeliveryTime.prototype, "time", {
    get: function() {
      return this.get("time");
    },
    set: function(val) {
      this.set("time", val);
    },
  });

  Object.defineProperty(DeliveryTime.prototype, "deletedAt", {
    get: function() {
      return this.get("deletedAt");
    },
    set: function(val) {
      this.set("deletedAt", val);
    },
  });

  Object.defineProperty(DeliveryTime.prototype, "isActive", {
    get: function() {
      return this.get("isActive");
    },
    set: function(val) {
      this.set("isActive", val);
    },
  });

  return DeliveryTime;
});
