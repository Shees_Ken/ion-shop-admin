angular.module('main').controller('NotificationCtrl',
  function ($scope, $translate, Auth, Notification, User, Item, Brand, Category, SubCategory, Toast) {

    $scope.notification = new Notification;

    $scope.notifications = [];

    Auth.ensureLoggedIn().then(function () {
      Notification.all().then(function (notifications) {
        $scope.notifications = notifications;
        $scope.$apply();
      });
    });

    $scope.onTypeChanged = function () {
      $scope.notification.item = null;
      $scope.notification.brand = null;
      $scope.notification.category = null;
      $scope.notification.subcategory = null;
    };

    $scope.queryItems = function (query) {
      var query = query || '';
      return Item.all({
        canonical: query.toLowerCase(),
        orderBy: 'asc',
        orderByField: 'name',
      });
    };

    $scope.queryBrands = function (query) {
      var query = query || '';
      return Brand.all({
        canonical: query.toLowerCase(),
        orderBy: 'asc',
        orderByField: 'name',
      });
    };

    $scope.queryCategories = function (query) {
      var query = query || '';
      return Category.all({
        canonical: query.toLowerCase(),
        orderBy: 'asc',
        orderByField: 'name',
      });
    };

    $scope.querySubCategories = function (query) {
      var query = query || '';
      return SubCategory.all({
        canonical: query.toLowerCase(),
        orderBy: 'asc',
        orderByField: 'name',
      });
    };

    $scope.queryUsers = function (query) {
      var query = query || '';
      var ids = $scope.notification.users.map(function (user) {
        return user.id
      });
      return User.all({
        canonical: query.toLowerCase(),
        anonymous: false,
        exclude: ids,
        orderBy: 'asc',
        type: 'customer',
        orderByField: 'name',
        limit: 10,
        page: 1,
      }).then(function (data) {
        return data.users;
      });
    };

    $scope.onSubmit = function () {

      $scope.isSending = true;

      Notification.save($scope.notification).then(function () {

        $translate('SENT').then(function (str) {
          Toast.show(str);
        });

        $scope.notifications.unshift($scope.notification);
        $scope.notification = new Notification;
        $scope.isSending = false;
        $scope.form.$setUntouched();
        $scope.form.$setPristine();

        $scope.$apply();

      }, function (error) {
        Toast.show(error.message);
        $scope.isSending = false;
        $scope.$apply();
      });
    }

  });