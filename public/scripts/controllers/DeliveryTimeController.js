angular.module('main').controller('DeliveryTimeCtrl',
  function (DeliveryTime, $scope, $mdDialog, $translate, Toast, Auth) {

    $scope.rowOptions = [5, 25, 50, 100];
    $scope.delivery_times = [];
    $scope.query = {
      canonical: '',
      limit: 25,
      page: 1,
      total: 0,
      onlyParent: false,
      parent: null,
    };

    $scope.onRefreshTable = function () {
      Auth.ensureLoggedIn().then(function () {
        $scope.promise = DeliveryTime.all($scope.query).then(function (times) {
          $scope.delivery_times = times;
          $scope.$apply();
        });
      });
    };

    $scope.onCountTable = function () {

      Auth.ensureLoggedIn().then(function () {
        $scope.promise = DeliveryTime.count($scope.query).then(function (total) {
          $scope.query.total = total;
          $scope.$apply();
        });

      });
    };

    $scope.onRefreshTable();
    $scope.onCountTable();

    $scope.onReload = function () {
      $scope.query.page = 1;
      $scope.onRefreshTable();
      $scope.onCountTable();
    };

    $scope.onReorder = function (field) {

      var indexOf = field.indexOf('-');
      var field1 = indexOf === -1 ? field : field.slice(1, field.length);
      $scope.query.orderBy = indexOf === -1 ? 'asc' : 'desc';
      $scope.query.orderByField = field1;
      $scope.onRefreshTable();
    };

    $scope.onPaginationChange = function (page, limit) {
      $scope.query.page = page;
      $scope.query.limit = limit;
      $scope.onRefreshTable();
    };

    $scope.onEdit = function (event, obj) {
      $mdDialog.show({
        controller: 'DialogDeliveryTimeController',
        scope: $scope.$new(),
        templateUrl: '/views/partials/delivery-time.html',
        parent: angular.element(document.body),
        locals: {
          obj: angular.copy(obj)
        },
        clickOutsideToClose: false

      }).then(function (response) {
        if (response) {
          $scope.onRefreshTable();
          $scope.onCountTable();
        }
      });
    };

    $scope.onDelete = function (event, obj) {

      $translate(['DELETE', 'CONFIRM_DELETE', 'CONFIRM', 'CANCEL', 'DELETED'])
        .then(function (str) {

          var confirm = $mdDialog.confirm()
            .title(str.DELETE)
            .textContent(str.CONFIRM_DELETE)
            .ariaLabel(str.DELETE)
            .ok(str.CONFIRM)
            .cancel(str.CANCEL);

          $mdDialog.show(confirm).then(function () {

            DeliveryTime.delete(obj).then(function () {
              $translate('DELETED').then(function (str) {
                Toast.show(str);
              });
              $scope.onRefreshTable();
              $scope.onCountTable();
              $scope.loadMainZones();
              $scope.loadSubZones();
            });
          });
        });
    }

    $scope.openMenu = function ($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

  }).controller('DialogDeliveryTimeController', function ($scope, $mdDialog, $translate, DeliveryTime, Toast, obj) {

    $scope.obj = obj || DeliveryTime.new();

    $scope.onCancel = function () {
      if ($scope.obj.dirty()) {
        $scope.obj.revert();
      }
      $mdDialog.cancel();
    };

    $scope.onSubmit = function () {

      $scope.isSaving = true;

      DeliveryTime.save($scope.obj).then(function (obj) {
        $scope.isSaving = false;
        $mdDialog.hide(obj);
        $scope.$apply();
        $translate('SAVED').then(function (str) {
          Toast.show(str);
        });
      }, function (error) {
        $scope.isSaving = false;
        $scope.$apply();
      });

    };

  })