const sharp = require('sharp')
const slug = require('limax')
const Utils = require('../utils')

Parse.Cloud.beforeSave('Banner', async (req) => {

    const obj = req.object
    const attrs = obj.attributes
    const user = req.user

    const isAdmin = await Utils.isAdmin(user)
    if (!isAdmin && !req.master) throw 'Not Authorized'

    if (!obj.existed()) {
        const acl = new Parse.ACL()
        acl.setPublicReadAccess(true)
        acl.setRoleWriteAccess('Admin', true)
        obj.setACL(acl)
    }

    if (obj.dirty('name')) {
        obj.set('canonical', attrs.name.toLowerCase())
        obj.set('slug', slug(attrs.name))
    }

    const image = attrs.image

    if (!image || !obj.dirty('image')) return

    let httpResponse = await Parse.Cloud.httpRequest({
        url: image.url()
    })

    let resizedImage = await sharp(httpResponse.buffer).resize(200, 200).toBuffer()

    var file = new Parse.File('photo.jpg', {
        base64: resizedImage.toString('base64')
    })

    let savedFile = await file.save()

    obj.set('imageThumb', savedFile)

})